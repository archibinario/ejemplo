const User = require('../../models/core/user');
const { generateTokenNumber, encrypt } = require('../../lib/helpers');
const { register, role } = require('../../services/index');

// devuelve todos los usuarios
exports.getAll = async (req, res, next) => {
    try {
        let data = await User
            .find()
            .populate('role', '-_id')
            .orFail(new Error('THERE_ARE_NO_RESULTS'));
        res.prepareResponse(req, res, next, data);
    } catch (error) {
        next(error);
    }
};
// devuelve un usuario
exports.getOne = async (req, res, next) => {
    try {
        let data = await User
            .findOne({ _id: req.params.id })
            .populate('role', '-_id')
            .orFail(new Error('THERE_ARE_NO_RESULTS'));
        res.prepareResponse(req, res, next, data);
    } catch (error) {
        next(error);
    }
};
// almacena un nuevo usuario desde el panel de control y mediante la acción de un administrador
exports.store = async (req, res, next) => {
    try {
        // obtengo el objeto user correctamente configurado enviando la plantilla de rol
        let user = await register.prepareUserData(req.body, req.body.template_role)
        // almaceno el usuario y la opción de enviar un email de verificación o no
        await register.newUser(user, req.body.send_email_verification);
        res.prepareResponse(req, res, next, data);
    } catch (error) {
        next(error);
    }
};
// genera de nuevo los 6 codigos de seguridad 2FA del usuario
exports.genBackup2FACodes = async (req, res, next) => {
    try {
        let codes = []
        for (let index = 1; index <= 6; index++) {
            codes.push({code: await generateTokenNumber(), created: Date.now()})
        }
        await User
            .findOneAndUpdate({ _id: req.params.id }, { two_factor_backup_codes: codes })
            .orFail(new Error('THERE_ARE_NO_RESULTS'));
        res.prepareResponse(req, res, next, codes);
    } catch (error) {
        next(error);
    }
};
// devuelve los 6 codigos de seguridad 2FA del usuario
exports.getBackup2FACodes = async (req, res, next) => {
    try {
        let data = await User
            .findOne({ _id: req.params.id })
            .select('two_factor_backup_codes')
            .orFail(new Error('THERE_ARE_NO_RESULTS'));
        res.prepareResponse(req, res, next, data);
    } catch (error) {
        next(error);
    }
};
// actualiza un usuario
exports.update = async (req, res, next) => {
    try {
        if (req.body && req.body.password) {
            req.body.password = await encrypt(req.body.password);
        }
        let data = await User
            .findOneAndUpdate({ _id: req.params.id }, req.body,{ new: true })
            .orFail(new Error('THERE_ARE_NO_RESULTS'));
        res.prepareResponse(req, res, next, data);
    } catch (error) {
        next(error);
    }
};
// cambia el rol de un usuario usando como base el nombre de la plantilla enviada
exports.change_role = async (req, res, next) => {
    try {
        if (!req.body || !req.body.template_role) {
            // no se envia body
            throw new Error('REQUEST_WITHOUT_DATA')
        }
        // obtengo el objeto que corresponde con el nombre de la plantilla enviada
        let template_role = await register.generateNewRole(req.body.template_role)
        // creo un nuevo rol en base a esa plantilla
        let r = await role.newRole(template_role)
        // meto los datos del nuevo rol en el objeto body usando el nombre de la propiedad
        // del modelo... asi se crea un "topadentro" ;)
        req.body.role = r._id
        await User
            .findOneAndUpdate({ _id: req.params.id }, req.body)
            .orFail(new Error('THERE_ARE_NO_RESULTS'));
        res.prepareResponse(req, res, next);
    } catch (error) {
        next(error);
    }
};
// hace un soft delete de la cuenta de un usuario
exports.delete = async (req, res, next) => {
    try {
        await User
            .findOneAndUpdate({ _id: req.params.id },
            {
                deleted: true,
                deleted_date: Date.now(),
                deleted_reason: req.body.reason
            })
            .orFail(new Error('THERE_ARE_NO_RESULTS'));
        res.prepareResponse(req, res, next);
    } catch (error) {
        next(error);
    }
};
