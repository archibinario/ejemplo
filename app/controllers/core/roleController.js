const Role = require('../../models/core/role')
const TemplateRole = require('../../models/core/templateRole')
const User = require('../../models/core/user')

exports.getAll = async (req, res, next) => {
    try {
        let data = await Role.find().orFail(new Error('THERE_ARE_NO_RESULTS'));
        res.prepareResponse(req, res, next, data);
    } catch (error) {
        next(error);
    }
};

exports.getOne = async (req, res, next) => {
    try {
        let data = await Role.findOne({ _id: req.params.id }).orFail(new Error('THERE_ARE_NO_RESULTS'));
        res.prepareResponse(req, res, next, data);
    } catch (error) {
        next(error);
    }
};

// creo un nuevo rol de usuario a partir del uuid de la plantilla enviada
exports.store = async (req, res, next) => {
    try {
        let templateRole = await TemplateRole
            .findOne({ _id: req.body.template })
            .orFail(new Error('THERE_ARE_NO_RESULTS'));
        // monto el nuevo rol para evitar colisiones con el objeto de mongoose
        let newRole = {
            name: req.body.name || templateRole.name,
            permissions: templateRole.permissions
        }
        await Role.create(newRole)
        res.prepareResponse(req, res, next);
    } catch (error) {
        next(error);
    }
};

// añade permiso a un rol
exports.update = async (req, res, next) => {
    try {
        // busco el rol mediante su id y asegurandome que no contiene ya
        // los permisos que me pasan en el request
        let role = await Role
            .findOne({ _id: req.params.id, permissions: {
                $not: { $elemMatch: { path: req.body.path, method: req.body.method } }
            }})
            .orFail(new Error('PERMISSION_ALREADY_SET'));
        // añado el nuevo permiso y guardo
        role.permissions.push({ path: req.body.path, method: req.body.method})
        await role.save()
        res.prepareResponse(req, res, next);
    } catch (error) {
        next(error);
    }
};

// elimina un permiso a un rol
exports.deleteSubDoc = async (req, res, next) => {
    try {
        await Role.findOneAndUpdate(
            {
                _id: req.params.id,
                permissions: { $elemMatch: { path: req.body.path, method: req.body.method } }
            },
            {
                $pull: { permissions: { path: req.body.path, method: req.body.method } }
            }
        ).orFail(new Error('THERE_ARE_NO_RESULTS'));
        res.prepareResponse(req, res, next);
    } catch (error) {
        next(error);
    }
};

// borra un rol
exports.delete = async (req, res, next) => {
    try {
        // si se elimina un rol, debe bloquearse el acceso del usuario que lo tiene asignado
        await Role.findOneAndDelete({ _id: req.params.id }).orFail(new Error('THERE_ARE_NO_RESULTS'));
        await User.findOneAndUpdate({ role: req.params.id }, { locked: true, locked_reason: 'role deleted'})
        res.prepareResponse(req, res, next);
    } catch (error) {
        next(error);
    }
};