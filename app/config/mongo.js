const mongoose = require('mongoose');
const CONFIG = require('./index').MONGO;
const {logger} = require('../services/index');
let dbConnectionReady = false;
let tick = null;
let URI = CONFIG.CONNECTION_STRING;

// configuración
// estableciendo el parametro useFindAndModify a true se evita el error
// "DeprecationWarning: collection.findAndModify is deprecated."
mongoose.set('useCreateIndex', true);
mongoose.set('useFindAndModify', false);
let options = {
    promiseLibrary: global.Promise,
    useNewUrlParser: true,
    auto_reconnect: CONFIG.AUTORECONNECT,
    reconnectTries: CONFIG.MRT,
    reconnectInterval: CONFIG.RTI
};

// listeners del objeto mongoose
mongoose.connection
    .on('error', (err) => {
        changeState(err.name, err.message);
    })
    .on('connected', () => {
        // elimino contador para que no vuelva a intentar reconexión dentro de 10 segundos
        clearInterval(tick);
        changeState('DB_MONGO_CONNECTED');
    })
    .on('reconnected', () => {
        changeState('DB_MONGO_RECONNECTED');
    })
    .on('reconnectFailed', () => {
        // avisar del error por email y comienzo de proceso de reconexion extra
        sendNotification();
        changeState('DB_MONGO_FAIL_RECONNECTION',`fail reconnection attemps, ${CONFIG.RTI} `);
    })
    .on('disconnected', () => {
        changeState('DB_MONGO_DISCONNECTED','database disconnected');
    });

// envia un email avisando que la base de datos esta caida y los intentos de reconexión
// automaticos han fallado. Ademas inicia un proceso de reconexiones extra
function sendNotification(){
    // TODO
    // enviar email de aviso
    console.log('envio del email');

    // iniciar intento de reconexión extra cada 10 segundos
    tick = setInterval(() => {
        connectDB();
    }, CONFIG.RTEI);
}

// cambia el estado de la conexión para aceptar o rechazar peticiones desde cliente
function changeState(name, err){
    if (err) {
        dbConnectionReady = false
        logger.error(name, { error: err, dbConnectionReady: dbConnectionReady })
    } else {
        dbConnectionReady = true
    }
}

// inicia la conexion con la db
async function connectDB() {
    await mongoose.connect(URI, options);
    return Promise.resolve()
};

// cierra la conexion con la db
function disconnectDB() {
    // el boolean indica si se fuerza [force] o no el cierre
    mongoose.connection.close(false);
};

// devuelve el estado de la conexión con la db
function dbReady() {
    return dbConnectionReady;
}

module.exports = { connectDB, disconnectDB, dbReady };
