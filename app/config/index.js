const config = {
    ENV: process.env.NODE_ENV,
    SETUP: JSON.parse(process.env.SETUP_INIT),
    ADMIN_EMAIL: process.env.ADMIN_EMAIL,
    ADMIN_NAME: process.env.ADMIN_NAME,
    ADMIN_SURNAME: process.env.ADMIN_SURNAME,
    LOG_PATH: process.env.LOG_PATH,
    LOG_FILE_NAME: process.env.LOG_FILE_NAME,
    INSTANCE: process.env.INSTANCE_ID,
    PORT: process.env.PORT,
    DOMAIN: process.env.DOMAIN,
    CURRENT_API_VERSION: process.env.CURRENT_API_VERSION,
    PROJECT_NAME: process.env.PROJECT_NAME,
    DEFAULT_LANGUAGE: process.env.DEFAULT_LANGUAGE,
    VALID_LANGUAGES: process.env.VALID_LANGUAGES,
    MULTILANGUAGE: JSON.parse(process.env.MULTILANGUAGE),
    RENDER_VIEWS: JSON.parse(process.env.RENDER_VIEWS),

    MONGO: {
        CONNECTION_STRING: process.env.MONGO_CONNECTION_STRING,
        AUTORECONNECT: process.env.MONGO_AUTORECONNECT,
        MRT: process.env.MONGO_MAX_RECONNECT_TRIES,
        RTI: process.env.MONGO_RECONNECT_INTERVAL,
    },

    REDIS: {
        CONNECTION_STRING: process.env.REDIS_URL
    },

    CACHE: {
        CACHE_ACTIVE: JSON.parse(process.env.CACHE_ACTIVE),
        PREFIX: process.env.CACHE_PREFIX,
        EXPIRATION: parseInt(process.env.CACHE_EXPIRATION)
    },

    REDIRECTION:{
        REDIRECTION_ACTIVE: JSON.parse(process.env.REDIRECTION_ACTIVE),
        REDIRECTION_PREFIX: process.env.REDIRECTION_PREFIX,
    },

    CSRF_COOKIE: process.env.CSRF_COOKIE,

    SESSION: {
        COOKIES: JSON.parse(process.env.SESSION_COOKIES),
        STORE: process.env.SESSION_STORE,
        URL: process.env.SESSION_URL_STORE,
        PREFIX: process.env.SESSION_PREFIX
    },

    JWT: {
        SECRET: process.env.JWT_SECRET,
        EXPIRATION: parseInt(process.env.JWT_EXPIRATION),
        EX: parseInt(process.env.JWT_EXPIRATION) + parseInt(process.env.JWT_EXPIRATION_STORE),
        PREFIX: process.env.JWT_PREFIX
    },

    COOKIE: {
        DOMAIN: process.env.DOMAIN,
        PATH: process.env.COOKIES_PATH,
        NAME: process.env.COOKIES_NAME,
        SECRET: process.env.COOKIES_SECRET,
        MAXAGE: parseInt(process.env.COOKIES_MAX_AGE),
        SECURE:  JSON.parse(process.env.COOKIES_SECURE),
        HTTPONLY:  JSON.parse(process.env.COOKIES_HTTP_ONLY),
        RESAVE_SESSION: JSON.parse(process.env.COOKIES_RESAVE_SESSION),
        SAVE_UNINITIALIZED_SESSION: JSON.parse(process.env.COOKIES_SAVE_UNINITIALIZED_SESSION),
    },

    SECURITY: {
        SECRET: process.env.SECRET,
        FRONT_IP: process.env.FRONT_IP,
        FRONT_TOKEN: process.env.FRONT_TOKEN,
        SALT: process.env.SALT,
        SALT_ROUNDS: parseInt(process.env.SALT_ROUNDS),
        _2FA_ENABLED: JSON.parse(process.env._2FA_ENABLED),
        _2FA_BACKUP_CODES_ENABLED: JSON.parse(process.env._2FA_BACKUP_CODES_ENABLED),
        _2FA_BACKUP_CODES_NUMBER: process.env._2FA_BACKUP_CODES_NUMBER,
        MAX_ATTEMPTS_LOGIN: process.env.MAX_ATTEMPTS_LOGIN,
        SUSPECT_PREFIX: process.env.SUSPECT_PREFIX,
        SUSPECT_BAN_ACTIVE: JSON.parse(process.env.SUSPECT_BAN_ACTIVE)
    },

    EMAIL_USE_SANDBOX: JSON.parse(process.env.EMAIL_USE_SANDBOX),

    EMAIL: {
        HOST: process.env.EMAIL_HOST,
        PORT: parseInt(process.env.EMAIL_PORT),
        USERNAME: process.env.EMAIL_USERNAME,
        PASSWORD: process.env.EMAIL_PASSWORD,
        TLS: JSON.parse(process.env.EMAIL_TLS),
        SSL: JSON.parse(process.env.EMAIL_SSL),
        REJECT_UNAUTHORIZED: JSON.parse(process.env.EMAIL_REJECT_UNAUTHORIZED),
    },

    EMAIL_SANDBOX: {
        HOST: process.env.EMAIL_SANDBOX_HOST,
        PORT: parseInt(process.env.EMAIL_SANDBOX_PORT),
        USERNAME: process.env.EMAIL_SANDBOX_USERNAME,
        PASSWORD: process.env.EMAIL_SANDBOX_PASSWORD,
        TLS: JSON.parse(process.env.EMAIL_SANDBOX_TLS),
        SSL: JSON.parse(process.env.EMAIL_SANDBOX_SSL),
        REJECT_UNAUTHORIZED: JSON.parse(process.env.EMAIL_SANDBOX_REJECT_UNAUTHORIZED),
    },

    FILTERS: {
        SIZE_LIMIT_IMAGE: process.env.IMAGE_SIZE_LIMIT,
        IMAGE_MIMES_VALID: process.env.IMAGE_MIMES_VALID,
        IMAGE_PATH_STORAGE: process.env.IMAGE_STORAGE
    }
};
//console.log(config.EMAIL)
module.exports = config;