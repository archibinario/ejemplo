const Router = require('express').Router();
const {COOKIES}  = require('../config').SESSION

/* CONTROLADORES */
    // publicos
    const loginController = require('../controllers/core/loginController');
    const registerController = require('../controllers/core/registerController');
    const resetPasswordController = require('../controllers/core/resetPasswordController');

    // privados
    const logoutcController = require('../controllers/core/logoutController');
    const cacheController = require('../controllers/core/cacheController');
    const redirectController = require('../controllers/core/redirectController');
    const endpointsController = require('../controllers/core/endpointsController');
    const roleController = require('../controllers/core/roleController');
    const templateRoleController = require('../controllers/core/templateRoleController');
    const userController = require('../controllers/core/userController');

/* MIDDLEWARES */
    const { isValidUri, cache, lang, redirect, auth, isOwner} = require('../middlewares/_index');
    let session = require('../middlewares/_index').jwt;     // middleware que verifica la sesión usando jwt
    if (COOKIES) {
        session = require('../middlewares/_index').cookie;  // si está activado la gestión de sesión mediante
                                                            // cookies, reemplazo el objeto validador de sesiones
    }
    const { validator } = require('../validators/index');   // middleware de validación de parametros y body

    Router.use(isValidUri);      // validación de la uri
    Router.use(lang);            // validación y extracción del idioma
    Router.use(redirect);        // comprobación de redirecciones

/* ENPOINTS PUBLICOS */
    // registro e inicio de sesión
    Router.get('/', (req, res, next) => {
    	res.prepareResponse(req, res, next,{message: 'hello'})
    })
    Router.post('/register', validator, registerController.register);
    Router.get('/validate/:token', validator, registerController.validateAccount);
    Router.post('/resend-email-validation', validator, registerController.resendValidateAccount);

    Router.post('/login', validator, loginController.login);
    Router.post('/login/2fa', validator, loginController._2fa);

    Router.post('/reset-password', validator, resetPasswordController.authRequestResetPassword);
    Router.get('/reset-password/:token', validator, resetPasswordController.validateTokenResetPassword);
    Router.put('/reset-password', validator, resetPasswordController.resetPassword);

    Router.use(cache);           // cache de uris publicas
    // contenidos publicos









    // contenidos publicos

/* ENPOINTS PRIVADOS */
    Router.use(session);    // control de autenticación de un JWT válido o de una cookie.

    // listado de endpoints
    Router.get('/endpoint', validator, auth, endpointsController.getAll);

    // gestión de cache
    Router.get('/cache-status', validator, auth, cacheController.status);
    Router.post('/cache-delete', validator, auth, cacheController.delete);
    Router.get('/cache-list', validator, auth, cacheController.list);
    Router.get('/cache-activate', validator, auth, cacheController.activate);
    Router.get('/cache-deactivate', validator, auth, cacheController.deactivate);
    Router.get('/cache-purge', validator, auth, cacheController.purge);

    // gestión de redirecciones
    Router.get('/redirect', validator, auth, redirectController.list);
    Router.post('/redirect', validator, auth, redirectController.save);
    Router.put('/redirect/:id', validator, auth, redirectController.updateRedirection);
    Router.delete('/redirect/:id', validator, auth, redirectController.delete);

    // gestion de roles
    Router.get('/role', validator, auth, roleController.getAll);
    Router.get('/role/:id', validator, auth, roleController.getOne);
    Router.post('/role', validator, auth, roleController.store);
    Router.put('/role/:id', validator, auth, roleController.update);
    Router.delete('/role/:id', validator, auth, roleController.delete);
    Router.delete('/role/:id/permission/', validator, auth, roleController.deleteSubDoc);

    // gestion de platillas de roles
    Router.get('/template-role', validator, auth, templateRoleController.getAll);
    Router.get('/template-role/:id', validator, auth, templateRoleController.getOne);
    Router.post('/template-role', validator, auth, templateRoleController.store);
    Router.put('/template-role/:id', validator, auth, templateRoleController.update);
    Router.delete('/template-role/:id', validator, auth, templateRoleController.delete);
    Router.delete('/template-role/:id/permission/', validator, auth, templateRoleController.deleteSubDoc);

    // ciere de sesión
    Router.get('/logout', logoutcController.logout);

    // gestión de usuarios
    // isOwner solo aplica sobre los endpoints a los que el usuario normal tiene acceso, y sirve para
    // confirmar que solo puede acceder a sus propios datos
    Router.get('/user', validator, auth, userController.getAll);
    Router.get('/user/:id', validator, auth, isOwner, userController.getOne);
    Router.post('/user', validator, auth, userController.store);
    Router.get('/user/:id/2fa-backup-codes', validator, auth, isOwner, userController.getBackup2FACodes);
    Router.put('/user/:id/2fa-backup-codes', validator, auth, isOwner, userController.genBackup2FACodes);
    Router.put('/user/:id', validator, auth, isOwner, userController.update);
    Router.put('/user/:id/change-role', validator, auth, userController.change_role);
    Router.delete('/user/:id', validator, auth, userController.delete);






    // ruta de prueba
    const User = require('../models/core/user')
    Router.get('/', validator, async (req, res, next)=> {
        let data = await User
            .find()
            .populate('role', '-_id -__v')
        res.prepareResponse(req, res, next, data, null, null, false);
    });

module.exports = Router;