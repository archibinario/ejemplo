exports.getErrorCodes = error_const => {
	const CODES_ERROR = {
        'CREDENTIALS_NOT_VALID': 401,
        'ACCOUNT_DISABLED': 403,
        'ERROR_VALIDATION_DATA': 422,                   // datos no validos para validacion
        'FIELD_MUST_BE_UNIQUE': 422,                    // datos no validos para monngose
        'VALUE_ALREADY_EXISTS': 422,                    // el valor enviado ya existe en db con unique
        'VALIDATION_ERROR': 422,                        // datos no validos para monngose
        'THERE_ARE_NO_RESULTS': 422,                    // no hay datos para la query enviada
        'PASSWORDS_DO_NOT_MATCH': 422,	                // password no coincide con password_confirm
        'FAIL_SEND_EMAIL': 422,
        'DATA_REQUIRED_NOT_SENDED': 422,                // datos necesarios para el proceso no se han enviado
        'LOGIN_FAIL': 403,                              // error de inicio de sesion
        'ACCOUNT_LOCKED': 403,                          // cuenta bloqueada por motivos administrativos
        'ACCOUNT_ALREADY_VALIDATED': 422,               // la cuenta ya ha sido validada y no es posinle reenviar email de validación
        'AUTORIZATION_HEADER_REQUIRED': 403,            // falta cabecera de autorización
        'AUTORIZATION_HEADER_BEARER_REQUIRED ': 403,    // la cabecera de autorización enviada no es del tipo indicado
        'TOKEN_INVALID_SIGNATURE': 403,                 // fallo en verificacion de la firmna token JWT
        'JWT_TOKEN_EXPIRED': 403,                       // token caducado
        'JWT_MALFORMED': 403,                           // datos en payload de jwt alterados
        'JWT_SIGNATURE_REQUIRED': 403,                  // error al verificar la autenticidad del token
        'JWT_INVALID_SIGNATURE': 403,                   // error al verificar la autenticidad del token
        'USER_DOES_NOT_EXISTS': 403,                    // token aparentemente valido pero usuario no encontrado en DB
        'TOKEN_NOT_VALID': 403,                         // la uuid asociada al token no se encuentra en redis, por lo tanto el token queda invalidado
        'ERROR_CREATING_SESSION': 500,
        'URL_NOT_FOUND': 404,
        'URI_FORMAT_NOT_VALID': 422,                    // la uri enviada no es valida por no cumplir con el requisito de longitud (menor o igual a 30 chars)
        'ACCOUNT_NOT_VALIDATED': 403,                   // intento de inicio de sesion sin haber validado la cuenta
        'ERROR_ON_RENEW_PASS': 400,                     // ha ocurrido un error en el proceso de recuperacion de contraseñas
        'PROCESS_RENEW_PASS_ABORTED': 403,              // ha habido un intento de recuperar la contraseña sin los token validos... ojo
        'UNAUTHORIZED': 401,                            // intento de entrada a la zona de admin sin los permisos necesarios
        'COOKIE_NOT_VALID': 403,                        // la cookie enviada tiene una ip distinta a la que inicio la sesión
        'FORMAT_FILE_NO_VALID': 422,                    // error proveniente de multer al intentar pasarle un fichero con formato no admitido por los filtros
        'MAX_FILE_SIZE_EXCEEDED': 422,                  // tamaño de fichero excede el maximo permitido
        'NO_FILE_SENDED': 422,                          // no se envia ningun fichero a multer
        'LIMIT_FILE_EXCEEDED': 422,                     // limite de envio de ficheros excedido
        'ONLY_FILES_ARE_ALLOWED': 422,                  // limite de multer alcanzado, ya que acepta un numnero determinado de archivos
        'FIELD_IDENTIFIER_NOT_VALID': 422,              // envio de un fichero usando un nombre de campo no valido
        'REQUEST_WITHOUT_DATA': 422,                    // no se envian datos para trabajar
        'CAST_ERROR': 422,                              // ha fallado la conversión de la ID de usuario al ObjectId
        'DB_DOWN': 500,                                 // base de datos caida, respuesta con estaticos
        '404': 404,
        'UUID_NOT_VALID': 422,                          // el identificador unico no cumple con el formato estandar de mongoose
        'EMAIL_ALREADY_REGISTERED': 422,                // la dirección de correo enviada en el registro ya está en uso
        '2FA_CODE_NOT_VALID': 403,                      // el codigo 2FA no es valido
        'PREAUTH_NOT_VALID': 403,                       // se da cuando un usuario intenta validar 2FA sin haberse logueado antes
        'FORBIDDEN': 403,                               // ocurre cuando se intenta acceder a un endpoint sin los permisos necesarios
        'BANNED': 403,                                  // se ha baneado al usuario por comportamiento sospechoso
        'PERMISSION_ALREADY_SET': 422                   // el permiso enviado para actualizar ya esta establecido
	}
	return CODES_ERROR[error_const];
}

exports.getSuccessCodes = success_const => {
    const CODES = {
        'LOGIN_SUCCESSFUL': 200,
        'ACCOUNT_CREATED_SUCCESSFUL': 201,
        'ACCOUNT_VALIDATED_SUCCESSFUL': 201,
        'LOGOUT_SUCCESSFUL': 200,
        'FORGOTPASS_PROCESS_STARTED': 200,
        'PROCESS_RENEW_PASS_ACCEPTED': 200,
        'RESOURCE_CREATED_SUCCESSFUL': 201,
        'RESOURCE_UPDATED_SUCCESSFUL': 200,
        'RESOURCE_DELETED_SUCCESSFUL': 200,
    }
    return CODES[success_const] || 200;
}

exports.normalizeErrorMsg = e => {
    const errorMessages = {
        'jwt expired': 'JWT_TOKEN_EXPIRED',
        'jwt malformed': 'JWT_MALFORMED',
        'jwt signature is required': 'JWT_SIGNATURE_REQUIRED',
        'invalid signature': 'JWT_INVALID_SIGNATURE',
        // multer
        'File too large': 'MAX_FILE_SIZE_EXCEEDED',
        'Unexpected field': 'FIELD_IDENTIFIER_NOT_VALID',
        'Too many fields': 'ONLY_FILES_ARE_ALLOWED',
        'Too many files':  'LIMIT_FILE_EXCEEDED',
        // mongoose
        'CastError': 'ERROR_ON_PROCESSING_REQUEST'
    }
    return errorMessages[e] || 'JWT_ERROR';
}


