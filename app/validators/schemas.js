const JoiBase = require('joi')
const { checkComplex } = require('./custom-validators')
const Joi = JoiBase.extend(checkComplex)


/* USER */
    const create_user = Joi.object({
        email: Joi.string().trim().required().email({ minDomainAtoms: 2 }).options({
            language: {
                any: {
                    required: '!!FIELD_IS_REQUIRED',
                },
                string: {
                    email: '!!FORMAT_NOT_VALID'
                }
            }
        }),
        password: Joi.string().trim().required().min(8).max(32).checkComplex().options({
            language: {
                any: {
                    required: '!!FIELD_IS_REQUIRED',
                },
                string: {
                    min: '!!PASSWORD_TOO_SHORT',
                    max: '!!PASSWORD_TOO_LONG',
                }
            }
        }),
        password_confirm: Joi.string().trim().required().valid(Joi.ref('password')).error(() => 'PASSWORDS_DO_NOT_MATCH'),
        policy_acepted: Joi.boolean().required().options({
            language: {
                any: {
                    required: '!!FIELD_IS_REQUIRED',
                },
                boolean: {
                    base: '!!FIELD_MUST_BE_TRUE'
                }
            }
        }),
        template_role: Joi.string(),
        send_email_verification: Joi.boolean().required()
    });
    const update_user = Joi.object({
        email: Joi.string().trim().email({ minDomainAtoms: 2 }).options({
            language: {
                string: {
                    email: '!!FORMAT_NOT_VALID'
                }
            }
        }),
        password: Joi.string().trim().min(8).max(32).checkComplex().options({
            language: {
                string: {
                    min: '!!PASSWORD_TOO_SHORT',
                    max: '!!PASSWORD_TOO_LONG',
                }
            }
        }),
        first_name: Joi.string().trim().max(32).options({
            language: {
                string: {
                    max: '!!FIRST_NAME_TOO_LONG',
                }
            }
        }),
        last_name: Joi.string().trim().max(32).options({
            language: {
                string: {
                    max: '!!LAST_NAME_TOO_LONG',
                }
            }
        }),
        password_confirm: Joi.string().trim().valid(Joi.ref('password')).error(() => 'PASSWORDS_DO_NOT_MATCH'),
        locked: Joi.boolean(),
        locked_reason: Joi.string().trim()
    })
    .with('locked', 'locked_reason').with('locked_reason', 'locked')
    .with('password', 'password_confirm').with('password_confirm', 'password');
    const change_role_user = Joi.object({
        template_role: Joi.string().required()
    })

module.exports = {
    'POST_/user': create_user,
    'PUT_/user/:id': update_user,
    'PUT_/user/:id/change-role': change_role_user
}