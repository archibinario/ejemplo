// custom validator que comprueba si la contraseña cumple con los requisitos de seguridad
// mayusculas, minusculas, numeros y espacios
exports.checkComplex = (joi) => {
    return {
        name: 'string',
        base: joi.string(),
        language: {
            checkComplex: 'PASSWORD_MUST_BE_COMPLEX'
        },
        rules: [{
            name: 'checkComplex',
            validate(params, value, state, options) {
                let password = value
                let hasUpperCase = /[A-Z]/.test(password);
                let hasLowerCase = /[a-z]/.test(password);
                let hasNumbers = /\d/.test(password);
                let hasSpaces = /\s/.test(password);
                if (hasUpperCase && hasLowerCase && hasNumbers && hasSpaces) {
                    return value;
                } else {
                    return this.createError('string.checkComplex', { value }, state, options);
                }
            }
        }]
    }
}