const Joi = require('joi')
const schemas = require('./schemas')
const MULTILENGUAGE = require('../config/index').MULTILANGUAGE

// Opciones de validación de Joi
const validationOptions = {
    abortEarly: false,      // detiene la validación en el primer error
    convert: true,          // convierte los datos a los valores requeridos
    //allowUnknown: false,  // acepta claves desconocidas que ignorará. Esta
    // opción junto a stripUnknown: true no tiene sentido, ya que stripUnknown: true por
    // defecto ya elimina las claves desconocidas
    stripUnknown: true,     // elimina elementos desconocidos de objectos y arrays
    escapeHtml: true        // escapa las entidades html en las respuestas de error
}
// función que se encarga de filtrar los parametros y validarlos (si los hay)
// ademas de crear la huella de la uri para llamar al schema de validación de
// body si fuera necesario.
// Tambien adapta los campos de body para el soporte multilenguaje (si esta activo)
exports.validator = async (req, res, next) => {
    try {
        // FLOW
        // - comprobar si hay parametros, si los hay
        // -- validar parametro token/id
        // -- filtrar token/id para obtener huella de la uri
        // - comprobar si hay datos en body y en tal caso, llamar al
        // - validador de body usando la huella de la uri obtenida y
        // validarlos usando la huella de la uri como identificador de schema

        // segmento la url enviada en partes, eliminando ademas los elementos vacios
        let parts = req.path.split('/').filter(function (el) {
            return el != '';
        });
        // compruebo si hay parametros en el request
        if (Object.keys(req.params).length > 0) {
            let response = await paramsValidator(req.params)
            // itero a traves del objeto de respuesta que contiene las claves y valores de los parametros enviados
            for (const key in response) {
                // key --> claves del los parametros enviados, por ejemplo: token, id
                // response[key] --> valor de cada clave
                // compruebo si el valor de cada clave está en el array de partes de la url
                let position = parts.indexOf(response[key])
                // en tal caso obtengo su posición y reemplazo el valor por el identificador del parametro
                // ejemplo: /api/v1/user/5c94b568cb63eb28707c5227 --> /api/v1/user/:id
                if( position != -1){
                    parts[position] = ':'+key
                }
            }
        }
        /* no hay parametros */

        // añado al objeto req los valores para que esten disponibles en toda la aplicación sin tener que importar mas dependencias
        // _clean_url --> objeto para determinar el permiso de acceso mas adelante (ACL)
        // _schema_validation --> identificador del schema de validación ejemplo: GET_api/v1/validate/:token/:id
        req._clean_url = { path: '/'+parts.join('/'), method: req.method }
        let schema_validation = req.method + '_' + req._clean_url.path

        /* console.log('parts ',parts)
        console.log('req._clean_url', req._clean_url)
        console.log('schema_validation', schema_validation) */



        // si se envia un POST, se pasa al validador
        if (req.method === 'POST') {
            await bodyValidator(req, schema_validation)
        }
        // compruebo si el DELETE viene con body, algunos traen data
        if (req.method === 'DELETE' && Object.keys(req.body).length != 0) {
            await bodyValidator(req, schema_validation)
        }
        // compruebo si el PUT viene con body, algunos traen data
        if (req.method === 'PUT' && Object.keys(req.body).length != 0) {
            await bodyValidator(req, schema_validation)
        }
        next()
    } catch (error) {
        next(error)
    }
}
// funcion que valida params en base al schema base de id y token
async function paramsValidator(data) {
    try {
        const schema = Joi.object({
            id: Joi.string().regex(/^[a-f\d]{24}$/i).error(() => 'FORMAT_FIELD_IS_NOT_VALID'),
            token: Joi.string().trim().token().length(64).error(() => 'FORMAT_FIELD_IS_NOT_VALID')
        });
        const result = await Joi.validate(data, schema, validationOptions)
        return Promise.resolve(result)
    } catch (error) {
        return Promise.reject(error)
    }
}
// funcion que valida el body en base al schema localizado a partir de la huella de la url
async function bodyValidator(req, schema_id){
    try {
        // compruebo si la petición trae un body
        if (Object.keys(req.body).length === 0) {
            throw new Error('REQUEST_WITHOUT_DATA')
        }
        // obtengo el body modificado por el validador (stripUnknown: true)
        let modifiedBody = await Joi.validate(req.body, schemas[schema_id], validationOptions)
        // guardo los datos validados en req.body
        if (MULTILENGUAGE) {
            // envio el nuevo body a la función que añade los identificadores de idioma
            req.body = await addLanguageIdentifier(modifiedBody)
        } else {
            req.body = modifiedBody
        }
        return Promise.resolve()
    } catch (error) {
        return Promise.reject(error)
    }
}
// función que convierte los campos con traducción en el nombre entendible por la db
// termina estando aqui como función post validación porque se deben transformar los
// campos despues de hacer la validación
async function addLanguageIdentifier (body) {
    try {
        let new_body = {}
        for (const key in body) {
            // compruebo si el nombre de la clave termina en guión bajo
            // esto indica que es un campo con traducción
            if (key.slice(-1) === '_') {
                // en tal caso almaceno la clave, añadiendo el identificador
                // de idioma al final, en el nuevo body
                new_body[key + __language] = body[key]
            } else {
                // si no tiene traducción, solo se almacena
                new_body[key] = body[key]
            }
        }
        return Promise.resolve(new_body)
    } catch (err) {
        return Promise.reject(new Error('ERROR_PARSE_DATA'))
    }
};