const jsonwebtoken = require('jsonwebtoken');
const CONFIG = require('../../config/index');
const {redis} = require('../../services/index');
const helpers = require('../../lib/helpers')


function normalizeErrorMsg(e) {
    const errorMessages = {
        'jwt expired'               : 'JWT_TOKEN_EXPIRED',
        'jwt malformed'             : 'JWT_MALFORMED',
        'jwt signature is required' : 'JWT_SIGNATURE_REQUIRED',
        'invalid signature'         : 'JWT_INVALID_SIGNATURE',
        'invalid token'             : 'JWT_MALFORMED'
    }
    return errorMessages[e] || 'JWT_ERROR';
}


// Middleware que extrae la cabecera con el JWT y lo procesa para conprobar su validez

module.exports = async function(req,res,next) {
	try {
        // extrae los datos de la cabecera
        let token_header = await extractHeaders(req)
        // comprueba si el token_header es valido y retorna el payload
        let token_id = await verifyToken(token_header);
        // busca el token_id extraido del payload en redis
        let t = await redis.get(CONFIG.JWT.PREFIX+token_id);
        // token no encontrado en redis
        //console.log('t --> ', JSON.parse(t))
        if (t === null) {
            throw new Error('TOKEN_NOT_VALID');
        }
        let temp_data = JSON.parse(t);
        // comprobar si la ip del request es la misma que la almacenada en el token
        if (req.ip !== temp_data.ip) {
            // no coincide la dirección IP, destruir token en redis
            await helpers.destroySession(token_id);
            // respondo con un mensaje de token no valido para no levantar sospechas,
            // el token es valido, pero la ip de la peticion no es la misma que la inicial
            throw new Error('TOKEN_NOT_VALID');
        }
        // todo correcto, almaceno al usuario en el objeto request
        req._user_data = {
            token_id: token_id,     // indica el id de sesión en redis
            uuid: temp_data.uuid,   // indica el uuid del usuario
            id: temp_data.id,       // indica el id (mongo) del usuario
            role: temp_data.role,    // indica el rol del usuario
            permissions: temp_data.permissions,
        }
        next();
    } catch (err) {
        next(err);
    }
};
// extrae los datos de la cabecera del request
function extractHeaders(req){
    return new Promise((resolve, reject) => {
        if (!req.headers.authorization) {
            return reject(new Error('AUTORIZATION_HEADER_REQUIRED'));
        }
        if (req.headers.authorization.split(' ')[0] !== 'Bearer') {
            return reject(new Error('AUTORIZATION_HEADER_BEARER_REQUIRED'));
        }
        // extrae el jwt de la parte de la cabecera
        let token_header = req.headers.authorization.split(" ")[1];
        if(!token_header){
            return reject(new Error('AUTORIZATION_HEADER_BEARER_REQUIRED'));
        }
        console.log('token_header--->', token_header)
        resolve(token_header);
    });
}
// verifica si el jwt es valido en base al secret definido
function verifyToken(token){
    return new Promise((resolve, reject) => {
        jsonwebtoken.verify(token, CONFIG.JWT.SECRET, function(err, payload) {
            if (err) {
                throw new Error(normalizeErrorMsg(err.message));
            }
            // payload.id almacena el token_id registrado en redis
            if (payload && payload.id && payload.id.length === 21) {
                //console.log('payload', payload)
                resolve(payload.id);
            } else {
                // por algun motivo el token_id ha sido alterado
                throw new Error(normalizeErrorMsg('JWT_MALFORMED'));
            }
        });
    });
}