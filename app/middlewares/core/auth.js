// comprueba si el usuario dispone de permisos sobre el endpoint solicitado
// el objeto req._user_data.permissions se crea durante el inicio de sesión
// y sus datos se almacenan en redis
module.exports = async function(req, res, next) {
	try {
        for (const permission of req._user_data.permissions) {
            // si tiene permiso sobre el endpoit y con el metodo indicado
            if (permission.path === req._clean_url.path && permission.method === req._clean_url.method) {
                return next()
            }
        }
        throw new Error('FORBIDDEN')
    } catch (err) {
        next(err);
    }
};