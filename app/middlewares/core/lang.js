const {DEFAULT_LANGUAGE, VALID_LANGUAGES, MULTILANGUAGE} = require('../../config/index')

// función que obtiene el lenguaje del request y establece la variable global __language
module.exports = async function(req, res,next){
	try {
        // si no esta activado el multilenguaje salto directamente
        global.__language = ''
        if (!MULTILANGUAGE) {
            return next()
        }
        // solicito los datos de las cabeceras validas
        // y añado el idioma escogido al objeto request
        // req.language = await extractHeaders(req)
        // agrega el idoma del request al objeto global, dispone en todos los modulos
        global.__language = await extractHeaders(req);
        next()
    } catch (err) {
        next(err);
    }
};
// lee las cabeceras Accept-Language y Language en busca de un idioma aceptado
function extractHeaders(req) {
    let language = req.get('Language') || req.get('Accept-Language')
    // en caso de que no se haya enviado ningun idioma en las cabeceras aceptadas,
    // se devuelve el idioma por defecto
    if (!language) {
        return Promise.resolve(DEFAULT_LANGUAGE)
    }
    // detecto si la cabecera viene con multiples opciones: es-ES,es;q=0.9,de;q=0.8,en;q=0.7,gl;q=0.6,it;q=0.5
    if (language.indexOf("-") != -1) {
        // en tal caso lipio la cadena quedandome com los dos primeros caracteres
        language = language.substring(0,2)
    }
    // busco la cadena del lenguaje enviado dentro del array de idiomas validos
    if (VALID_LANGUAGES.includes(language)) {
        return Promise.resolve(language.toLowerCase())
    }
    // si no se ha enviado un idioma valido, se escoge el idioma por defecto
    return Promise.resolve(DEFAULT_LANGUAGE)
}