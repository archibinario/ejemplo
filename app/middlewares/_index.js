const middlewares = {
    jwt: require('./core/jwt'),                 // verifica el JWT de cada request
    cookie: require('./core/cookie'),           // verifica la cookie de cada request
    isValidUri: require('./core/isValidUri'),   // valida que la uri del request sea valida
    cache: require('./core/cache'),             // comprueba si el request está cacheado
    lang: require('./core/lang'),               // realiza tareas relacionadas con el multilenguaje
    redirect: require('./core/redirect'),       // comprueba si el request apunta una uri con redirección
    suspect: require('./core/suspect'),         // realiza comprobaciones de seguridad
    isOwner: require('./core/isOwner'),         // comprueba si el recurso solicitado pertenece al usuario que lo solicita
    auth: require('./core/auth')                // comprueba si se poseen permisos sobre el endpoint solicitado
};

module.exports = middlewares;