const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const backupCodeSchema = new Schema({
    code: { type: String, required: true },
    valid: { type: Boolean, default: true },
    used: { type: String, default: 'none' },
    created: { type: String, default: Date.now() }
}, { _id: false })

const schema = new Schema({
    /* basicos */
    user_id: {type: String, index: true},
    email: {type: String, index: true, lowercase: true},
    password: { type: String, select: false},
    policy_acepted: {type: Boolean},

    /* automaticos */
    token_reset_password:{type: String, default: null, select: false},
    token_validation: {type: String, default: null, select: false},
    account_validated: {type: Boolean, default: false},
    first_login: {type: Boolean, default: true},
    two_factor_secret: {type: String, default: null, select: false},
    two_factor_qr_url: {type: String, default: null, select: false},
    two_factor_backup_codes: {type: [backupCodeSchema], select: false},

    locked: {type: Boolean, default: false},            // cuenta bloqueada
    locked_reason: { type: String },
    deleted: {type: Boolean, default: false},           // cuanta eliminada
    deleted_date: Date,
    deleted_reason: {type: String},

    /* perfil */
    first_name: {type: String, default: null},
    last_name: {type: String, default: null},
    avatar: {type: String, default: null},
    role: { type: mongoose.Schema.Types.ObjectId, ref: 'Role', required: true, select: false },
    __v: { type: Number, select: false }
},{timestamps: true});

// este metodo se usa para buscar un email en db y comprobar si ya esta
// o ha estado dado de alta valiendose de la propiedad 'active'
schema.statics.checkEmail = async function (email) {
    var data = await this.findOne({ email: email, deleted: false })
    if(data){
        return Promise.reject(new Error('EMAIL_ALREADY_REGISTERED'))
    } else {
        return Promise.resolve()
    }
}

module.exports = mongoose.model('User', schema,'users');