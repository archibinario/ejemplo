const mongoose = require('mongoose');

const Schema = mongoose.Schema;
const PermissionSchema = new Schema({
    path: { type: String, required: true },
    method: { type: String, required: true }
}, { _id: false } )

const schema = new Schema({
    name: { type: String, default: 'user' },
    permissions: { type: [PermissionSchema] },
    __v: { type: Number, select: false }
});

module.exports = mongoose.model('Role', schema,'roles');