const redis = require('redis');
const {promisify} = require('util');
const CONFIG = require('../../config/index').REDIS
const logger = require('../logger/index');

const client = redis.createClient({
    url: CONFIG.CONNECTION_STRING
})

const get = promisify(client.get).bind(client);
const keys = promisify(client.keys).bind(client);
const set = promisify(client.set).bind(client);
const del = promisify(client.del).bind(client);
const sadd = promisify(client.sadd).bind(client);
const smembers = promisify(client.smembers).bind(client);
const srem = promisify(client.srem).bind(client);
const flushdb = promisify(client.flushdb).bind(client);
const incr = promisify(client.incr).bind(client);

client.on('reconnecting', function() {
    console.log('REDIS_RECONNECTING');
});

client.on('error', function(err) {
    logger.error('REDIS_ERROR', { error: err })
    console.log(err)
    throw new Error(err)
});

module.exports = { get, keys, set, del, flushdb, sadd, smembers, srem, incr};