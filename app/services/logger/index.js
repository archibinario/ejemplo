const pino = require('pino');
// el log va configurado con logrotate...

const {LOG_PATH, LOG_FILE_NAME} = require('../../config/index')
const stream = pino.destination(`${LOG_PATH}${LOG_FILE_NAME}.log`);
const logger = pino(stream);
module.exports = logger;