const services = {
    logger: require('./logger/index'),
    redis: require('./redis/redis-async'),
    multer: require('./multer/image'),
    cache: require('./cache/index'),
    email: require('./email/index'),
    register: require('./register/index'),
    reset_password: require('./reset-password/index'),
    role: require('./role/index'),
};

module.exports = services;