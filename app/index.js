/* MODULOS */
const express = require('express');
const app = express();
const errors = require('./lib/errors-handlers');
const { successResponse } = require('./lib/response');
const { PROJECT_NAME, RENDER_VIEWS, ENV, PORT, SESSION, SETUP } = require('./config/');
const { MESSAGES } = require('./lang/messages')
const init = require('./config/init');
const { connectDB, disconnectDB, dbReady } = require('./config/mongo');
const { suspect } = require('./middlewares/_index');
let sessionConfig;
const cors = require('cors')
const apiv1 = require('./routes/v1');


/* MIDDLEWARES */
app.use(express.urlencoded({ extended: true }));
app.use(express.json());
app.use(cors())
app.use(suspect)      // middleware que se encarga de controlar si el request está pendiente de baneo

/* CONFIGURACIÓN */
global.__basedir = __dirname;    // agrega el rootpath al objeto global, dispone en todos los modulos

if (SESSION.COOKIES) {
    const session = require('express-session');
    sessionConfig = require('./config/session');
    app.use(session(sessionConfig));
}

// se usa el renderizado de vistas
if (RENDER_VIEWS) {
    const exphbs = require('express-handlebars');
    const hbsHelpers = require('./lib/hbs-helpers');
    app.engine('.hbs', exphbs({
        extname: '.hbs',
        layoutsDir: __dirname + '/views/layouts/',
        partialsDir: __dirname + '/views/partials/',
        defaultLayout: 'main',
        helpers: hbsHelpers
    }));
    app.set('views', __dirname + '/views');
    app.set('view engine', '.hbs');
}

if (ENV === 'production') {
    // cosas de produccion...
    app.set('trust proxy', 1) // proxy inverso

    // si se esta usando un gestor de sesiones basado en cookies
    if (sessionConfig.cookie) {
        sessionConfig.cookie.secure = true
    }
}
// configuración general
app.use((req, res, next) => {
    // determina si la base de datos está caida y devuelve un estatico con 500
    if (dbReady() === false) {
        // no hay conexión con la base de datos
        return next('DB_DOWN')
    }
    // carga de datos estaticos para las vistas
    if (RENDER_VIEWS) {
        const static_data = {
            project_name: PROJECT_NAME,
            year: new Date().getFullYear()
        }
        res.locals.static_data = static_data;
    }
    // middleware que añade un objeto de respuesta estandar al objeto res
    // esto se usa para responder desde un mismo punto y no desde cada manejador de ruta
    res.prepareResponse = successResponse;
    next();
});

/* RUTAS */
app.use('/api/v1', apiv1)



// RUTA DE PRUEBAS
app.get('/prueba', async (req, res, next) => {
    if (req.session._user_data) {
        console.log(req.session._user_data)
    } else {
        const User = require('./models/core/user')
        let user = await User.findOne({ _id: '5ca89a90907fcd7405a454cd'}).populate('role')
        let userSession = {
            uuid: user.user_id,
            id: user._id,
            role: user.role.name,
            ip: req.ip,
            permissions: user.role.permissions
        }
        req.session._user_data = userSession;
    }
    res.render('public/test',{layout: 'empty'})
})

// 404
app.use((req, res, next) => {
    throw new Error('URL_NOT_FOUND');
})

/* MANEJO DE ERRORES */
app.use(errors.errorHandler);
app.use(errors.logErrors);
app.use(errors.errorResponse);

/* LEVANTAR SERVER */
const server = app.listen(PORT, async () => {
    try {
        console.log(`APP_LISTENING_PORT ${PORT} ON ${ENV}`);
        // ejecuto script de inicialización
        // app es necesrio para sacar la lista de endpoints
        await init.options(app);
        await connectDB();
        if (SETUP === true) {
            console.log(`INICIANDO EL SETUP INICIAL`);
            const { setup } = require('./setup')
            setup(app)
        }
    } catch (error) {
        throw new Error('FAIL_STARTING_APP')
    }

});

/* MANEJADOR DE ERRORES NO CONTROLADOS */
process.on('uncaughtException', function (err) {
    if (err.message.includes("EACCES") && err.message.includes("/var/log/")) {
        console.log(MESSAGES.ERROR_MESSAGE_LOG_FILE_MISSING)
    }
    if (err.message.includes("not found") && err.message.includes("/bin/sh")) {
        console.log(MESSAGES.ERROR_MESSAGE_BASH_SCRIPT_NOT_FOUND)
    }
    if (err.message.includes("Redis") && err.message.includes("ECONNREFUSED")) {
        console.log(err.message)
    }

    console.log('exepción no manejada:', err)
    closeApp()      // salimos del proceso de node de forma ordenada
});

/* PROCESO CIERRE ORDENADO */
process.on('SIGTERM', closeApp);    // cierre del proceso desde el gestor del sistema
process.on('SIGINT', closeApp);     // cierre ctrl + c

function closeApp(){
    console.info('Close signal received. Closing http server.');
    server.close(() => {
        console.info('HTTP_SERVER_CLOSED');
        disconnectDB();
        console.info('DB_CONNECTION_CLOSED');
        process.exit(0);
    });
}
